FROM node:10.15.3-alpine

# Create app directory
WORKDIR /usr/src/app

# Install app dependencies
# A wildcard is used to ensure both package.json AND package-lock.json are copied
# where available (npm@5+)
COPY . /usr/src/app/

RUN npm i npm@latest -g
RUN npm i
RUN npm run build

# If you are building your code for production
# RUN npm ci --only=production


EXPOSE 80
CMD [ "npm", "start" ]
