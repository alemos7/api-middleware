# Corteva SSO Middleware Endpoints

API Documentation

## User endpoints

### Login Endpoint
* Path: `/login` 
* Method: `POST`
* Request Headers:
  * Content-Type: application/json
  * Authorization: Bearer `TOKEN`

Request Body:

```json
{
	"payload": {
		"username": "string",
		"password": "string"
	},
	"callback_url": "string"
}
```
Response:
```json
{
    "user_id": 1, // unique identifier numeric
    "user": {
        "username": "string",
        "firstname": "string",
        "lastname": "string"
    },
    "access": true // true or false
    "redirect_url": "string", // register / forgot password / checkout // if access is "true" this redirect will not used, if "false" the redirct must be automatically from Flights.
    "token": "string"
}
```

Response Error:
```json
{
    "error_message": "string" 
    "redirect_url": "string"
}
```

### Validate Token Endpoint
* Path: `/validate/token` 
* Method: `POST`
* Request Headers:
  * Content-Type: application/json
  * Authorization: Bearer `TOKEN`

Request:
```json
{
    "user_id": 1, // unique identifier numeric
    "token": "string"
}
```

### Register Endpoint
* Path: `/register` 
* Method: `POST`
* Request Headers:
  * Content-Type: application/json
  * Authorization: Bearer `TOKEN`

Request Body:

```json
{
    "user": {
        "email": "string",
        "password": "string",
        // ... another arguments required
    },
    "provider_identifier": "string code",
    "callback_url": "string" // optional
}
```
Response:
```json
{
    "user_id": 1, // unique identifier numeric
    "user": {
        "username": "string",
        "firstname": "string",
        "lastname": "string"
    },
    "access": true // true or false
    "redirect_url": "string", // register / forgot password / checkout
}
```

### Forgot Password Endpoint
* Path: `/forgot-password` 
* Method: `POST`
* Request Headers:
  * Content-Type: application/json
  * Authorization: Bearer `TOKEN`

Request Body:

```json
{
    "user": {
        "email": "string",
    },
    "provider_identifier": "string code"
}
```
Response: Status Code HTTP

### Access Type:
|  Access Type Option|Description|
|--|--|
| free |  Free basic access|
| full_access | Full Paid Access |

### Provider Identifier:
|  Service Provider Option|Description|
|--|--|
| mi_lote |  Mi Lote|
| corteva_fligths |  Corteva Flights|

### Cases when the redirect url appears:
|  Case| redirect_url |Description |
|--|--|--|
| Succesfull Login & Paid | No callback | No description |
| Failed Login - Bad Credentials | SSO-Pass |Password Recovery Page|
| Succesfull Login & Not Paid | SSO-Paid |Checkout Page|
| User not Registered  | SSO-Reg | Register Page|

## CRUD Providers (Internal Endpoints)

### CREATE Provider
* Path: `/providers` 
* Method: `POST`
* Request Headers:
  * Content-Type: application/json
  * Authorization: Bearer `TOKEN`

Request Body:

```json
{
    "name":"string",
    "code":"string",
    "configurations": [
        {
            "key": "string",
            "value": "String or Object"
        },
        {
            "key": "string",
            "value": "String or Object"
        }
    ]
}
```

Response Body:

```json
{
    "id": "numeric",
    "name":"string",
    "code":"string",
    "configurations": [
        {
            "key": "string",
            "value": "String or Object"
        },
        {
            "key": "string",
            "value": "String or Object"
        }
    ]
}
```

### UPDATE Provider
* Path: `/providers/{id}` 
* Method: `PUT`
* Request Headers:
  * Content-Type: application/json
  * Authorization: Bearer `TOKEN`

Request Body:

```json
{
    "name":"string",
    "code":"string",
    "configurations": [
        {
            "key": "string",
            "value": "String or Object"
        }
    ]
}
```

Response Body:

```json
{
    "id": "numeric",
    "name":"string",
    "code":"string",
    "configurations": [
        {
            "key": "string",
            "value": "String or Object"
        },

            "key": "string",
            "value": "String or Object"
        }
    ]
}
```

### GET Provider
* Path: `/providers/{id}` 
* Method: `GET`
* Request Headers:
  * Content-Type: application/json
  * Authorization: Bearer `TOKEN`

Response Body:

```json
{
    "id": "numeric",
    "name":"string",
    "code":"string",
    "configurations": [
        {
            "key": "string",
            "value": "String or Object"
        },
        {
            "key": "string",
            "value": "String or Object"
        }
    ]
}
```

### GET List of Providers
* Path: `/providers` 
* Method: `GET`
* Request Headers:
  * Content-Type: application/json
  * Authorization: Bearer `TOKEN`

Response Body:

```json
[
    {
        "id": "numeric",
        "name":"string",
        "code":"string",
        "configurations": [
            {
                "key": "string",
                "value": "String or Object"
            },
            {
                "key": "string",
                "value": "String or Object"
            }
        ]
    },
    {
        "id": "numeric",
        "name":"string",
        "code":"string",
        "configurations": [
            {
                "key": "string",
                "value": "String or Object"
            },
            {
                "key": "string",
                "value": "String or Object"
            }
        ]
    },
    // ....
]
```

### DELETE Provider
* Path: `/providers/{id}` 
* Method: `DELETE`
* Request Headers:
  * Content-Type: application/json
  * Authorization: Bearer `TOKEN`
* Response: HTTP Status Code
