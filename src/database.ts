import mongoose, {Mongoose} from 'mongoose';
import {inject, injectable} from "inversify";
import Logger, {LoggerType} from "@generics/logger";

@injectable()
export class Database {
    private static readonly CONNECT_CHANCES = +process.env.MONGODB_CONNECTION_RETIES || 5;

    constructor(
        @inject(LoggerType) private logger: Logger,
    ) {
    }

    private static getMongoUrl(): string {
        return process.env.MONGODB_URL || 'mongodb://corteva:corteva@mongodb.internal:27017/corteva';
    }

    async connect(): Promise<Mongoose> {
        mongoose.Promise = global.Promise;
        let chancesLeft = Database.CONNECT_CHANCES;
        while (chancesLeft > 0) {
            try {
                this.logger.info("Connecting to Mongodb...");
                return await mongoose.connect(Database.getMongoUrl(), {
                    useNewUrlParser: true,
                    useUnifiedTopology: true,
                    useCreateIndex: true
                })
            } catch (e) {
                chancesLeft--
                if (chancesLeft !== 0) {
                    this.logger.warn(`Failed to connect to Mongodb ${chancesLeft} retries left`)
                }
            }
        }
        throw new Error(`Mongodb failed to connect after ${Database.CONNECT_CHANCES} retries`)
    }
}