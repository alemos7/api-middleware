import 'module-alias/register';
import {Router} from 'express'
import di from '@di/index'
import {Ping} from "@useCase/tools/ping";
import "reflect-metadata";
import {handleUseCase} from "@generics/handleUseCase";

const router = Router();

router.get("/", (req, res) => {
    res.json({message: 'Welcome to the Api'});
});

router.get("/ping", (req, res) => {
    const useCase: Ping = di.getContainer().get<Ping>("Ping");
    handleUseCase(Promise.all([useCase.get()]), req, res)
});

export default router;
