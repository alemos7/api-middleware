import "reflect-metadata";
import {Router} from 'express'
import di from "@di/index";
import {RegisterProvider} from "@useCase/provider/register.provider";
import {RetrieveProviders, RetrieveProvidersType} from "@useCase/provider/retrieve.providers";
import {LoginProvider, LoginProviderType} from "@useCase/provider/login.provider";
import {validateSelfProvider} from "@configuration/middleware/provider.middleware";
import {RetrieveProviderByCode, RetrieveProviderByCodeType} from "@useCase/provider/retrieve.provider.by.code";
import {handleUseCase} from "@generics/handleUseCase";

const router = Router();

router.get("/provider", validateSelfProvider, (req: any, res: any) => {
    res.json(res.locals.provider)
});

router.get("/providers", validateSelfProvider, (req: any, res: any) => {
    const useCase: RetrieveProviders = di.getContainer().get<RetrieveProviders>(RetrieveProvidersType);
    handleUseCase(useCase.get(), req, res)
});

router.get("/providers/:code", validateSelfProvider, (req: any, res: any) => {
    const useCase: RetrieveProviderByCode = di.getContainer().get<RetrieveProviderByCode>(RetrieveProviderByCodeType);
    handleUseCase(useCase.get(req.params.code), req, res)
});

router.post("/provider/signup", validateSelfProvider, (req: any, res: any) => {
    const useCase: RegisterProvider = di.getContainer().get<RegisterProvider>("RegisterProvider");
    handleUseCase(useCase.execute(req.body), req, res)
});

router.post("/provider/login", (req, res) => {
    const useCase: LoginProvider = di.getContainer().get<LoginProvider>(LoginProviderType);
    handleUseCase(useCase.execute(req.body), req, res)
});

export default router;
