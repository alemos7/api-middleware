import {Router} from "express";
import {setSelfProvider} from "@configuration/middleware/provider.middleware";
import {UserLogin, UserLoginType} from "@useCase/user/user.login";
import di from "@di/index";
import {handleUseCase} from "@generics/handleUseCase";
import {UserRegister, UserRegisterType} from "@useCase/user/user.register";
import {UserResetPassword, UserResetPasswordType} from "@useCase/user/user.reset.password";

const router = Router();

router.post("/login", setSelfProvider, (req: any, res: any) => {
    const useCase: UserLogin = di.getContainer().get<UserLogin>(UserLoginType);
    handleUseCase(useCase.execute(req.body, res.locals.provider), req, res)
});

router.post("/register", setSelfProvider, (req: any, res: any) => {
    const useCase: UserRegister = di.getContainer().get<UserRegister>(UserRegisterType);
    handleUseCase(useCase.execute(req.body, res.locals.provider), req, res)
})

router.post("/forgot-password", setSelfProvider, (req: any, res: any) => {
    const useCase: UserResetPassword = di.getContainer().get<UserResetPassword>(UserResetPasswordType);
    handleUseCase(useCase.execute(req.body), req, res)
})

export default router