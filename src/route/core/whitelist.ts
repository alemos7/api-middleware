import {validateLoggedProvider} from "@configuration/middleware/provider.middleware";
import {UserWhitelistExists, UserWhiteListExistsType} from "@useCase/user/whitelist/user.whitelist.exists";
import di from "@di/index";
import {handleUseCase} from "@generics/handleUseCase";
import {UserWhitelistCreate, UserWhitelistCreateType} from "@useCase/user/whitelist/user.whitelist.create";
import {Router} from "express";
import {UserWhitelistList, UserWhiteListListType} from "@useCase/user/whitelist/user.whitelist.list";
import {UserWhitelistDelete, UserWhitelistDeleteType} from "@useCase/user/whitelist/use.whitelist.delete";

const router = Router();

/**
 * List all whitelists
 * @GET /whitelist/
 */
router.get("/whitelist", validateLoggedProvider, (req: any, res: any) => {
    const useCase: UserWhitelistList = di.getContainer().get<UserWhitelistList>(UserWhiteListListType);
    handleUseCase(useCase.get(), req, res)
})

/**
 * Whitelist Exists?
 * @GET /whitelist/:provider/:code
 */
router.get("/whitelist/:provider/:code", validateLoggedProvider, (req: any, res: any) => {
    const useCase: UserWhitelistExists = di.getContainer().get<UserWhitelistExists>(UserWhiteListExistsType);
    handleUseCase(useCase.get(req.params.code, req.params.provider), req, res)
})

/**
 * Create Whitelist
 * @POST /whitelist
 */
router.post("/whitelist", validateLoggedProvider, (req: any, res: any) => {
    const useCase: UserWhitelistCreate = di.getContainer().get<UserWhitelistCreate>(UserWhitelistCreateType);
    handleUseCase(useCase.execute(req.body), req, res)
})

/**
 * Delete whitelist
 * @DELETE /whitelist/:provider/:code
 */
router.delete("/whitelist/:provider/:code", validateLoggedProvider, (req: any, res: any) => {
    const useCase: UserWhitelistDelete = di.getContainer().get<UserWhitelistDelete>(UserWhitelistDeleteType);
    handleUseCase(useCase.execute(req.params.code, req.params.provider), req, res)
})

export default router;