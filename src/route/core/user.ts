import 'module-alias/register';
import {Router} from 'express'
import di from '@di/index'
import "reflect-metadata";
import {UserLogin, UserLoginType} from "@useCase/user/user.login";
import {validateLoggedProvider} from "@configuration/middleware/provider.middleware";
import {UserRegister, UserRegisterType} from "@useCase/user/user.register";
import {UserTokenActivate, UserTokenActivateType} from "@useCase/user/user.token.activate";
import {UserResetPassword, UserResetPasswordType} from "@useCase/user/user.reset.password";
import {handleUseCase} from "@generics/handleUseCase";
import {UserTokenValidate, UserTokenValidateType} from "@useCase/user/user.token.validate";

const router = Router();

router.post("/user/login", validateLoggedProvider, (req: any, res: any) => {
    const useCase: UserLogin = di.getContainer().get<UserLogin>(UserLoginType);
    handleUseCase(useCase.execute(req.body, res.locals.provider), req, res)
});

router.post("/user/register", validateLoggedProvider, (req: any, res: any) => {
    const useCase: UserRegister = di.getContainer().get<UserRegister>(UserRegisterType);
    handleUseCase(useCase.execute(req.body, res.locals.provider), req, res)
})

router.post("/user/validate-token", validateLoggedProvider, (req: any, res: any) => {
    const useCase: UserTokenValidate = di.getContainer().get<UserTokenValidate>(UserTokenValidateType);
    handleUseCase(useCase.execute(req.body, res.locals.provider), req, res)
})

router.post("/user/activate/:token/:salt", validateLoggedProvider, (req: any, res: any) => {
    const useCase: UserTokenActivate = di.getContainer().get<UserTokenActivate>(UserTokenActivateType);
    handleUseCase(useCase.execute(req.params.token, req.params.salt, req.body), req, res)
})

router.post("/user/forgot-password", validateLoggedProvider, (req: any, res: any) => {
    const useCase: UserResetPassword = di.getContainer().get<UserResetPassword>(UserResetPasswordType);
    handleUseCase(useCase.execute(req.body), req, res)
})

export default router;
