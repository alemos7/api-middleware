import 'module-alias/register';
import dotenv from 'dotenv';
import Di from "@di/index";
import {bindDefaults} from "@configuration/di";
import Logger, {LoggerType} from "@generics/logger";
import {App} from './app';
import {Database} from "./database";


async function main() {
    dotenv.config()
    const container = Di.getContainer();
    const logger = container.get<Logger>(LoggerType);
    logger.debug("Binding Core Defaults")
    bindDefaults();
    try {
        const app = container.get<App>(App);
        const db = container.get<Database>(Database);
        await db.connect();
        await app.listen();
    } catch (e) {
        logger.error(e.message);
        process.exit(1)
    }
}

main();
