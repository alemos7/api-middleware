import ReadInterface from "@repositories/base/read.interface";
import WriteInterface from "@repositories/base/write.interface";
import {UserWhitelist} from "@entities/user/whitelist/model";

export const UserWhitelistRepositoryType = Symbol("UserWhitelistRepository")

export interface UserWhitelistRepository extends ReadInterface<UserWhitelist>, WriteInterface<UserWhitelist> {
    exists(email: string, providerCode: string): Promise<boolean>

    deleteByEmailAndProviderCode(email: string, providerCode: string): Promise<boolean>
}