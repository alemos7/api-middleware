import {injectable} from "inversify";
import MongooseRepository from "@repositories/base/mongoose.repository";
import UserWhitelistModel, {UserWhitelist} from "@entities/user/whitelist/model";
import {UserWhitelistRepository} from "@repositories/user/whitelist/user.whitelist.repository";
import {HttpError} from "../../../../exceptions/http.error";
import {NOT_FOUND} from "http-status-codes";

@injectable()
export class DefaultUserWhitelistRepository extends MongooseRepository<UserWhitelist> implements UserWhitelistRepository {
    constructor() {
        super()
        this.model = UserWhitelistModel
    }

    async exists(email: string, providerCode: string): Promise<boolean> {
        const exists = await this.model.findOne({
            email,
            provider: providerCode,
        })
        return Promise.resolve(!!exists)
    }

    async deleteByEmailAndProviderCode(email: string, providerCode: string): Promise<boolean> {
        const deletedModel = await this.model.findOneAndRemove({
            email,
            provider: providerCode,
        })
        if (!deletedModel) {
            throw new HttpError(NOT_FOUND, `${this.model.modelName} [email: ${email}, provider: ${providerCode}] not found`)
        }

        return Promise.resolve(true)
    }
}