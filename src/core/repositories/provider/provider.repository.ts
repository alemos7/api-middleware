import {Provider} from "@entities/provider/model";
import ReadInterface from "@repositories/base/read.interface";
import WriteInterface from "@repositories/base/write.interface";

export const ProviderRepositoryType = Symbol("ProviderRepository")

export interface ProviderRepository extends ReadInterface<Provider>, WriteInterface<Provider> {
    findByCode(code: string): Promise<Provider>
}