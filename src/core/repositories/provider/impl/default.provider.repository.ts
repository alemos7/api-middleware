import {injectable} from "inversify";
import ProviderModel, {Provider} from "@entities/provider/model";
import {ProviderRepository} from "@repositories/provider/provider.repository";
import MongooseRepository from "@repositories/base/mongoose.repository";

@injectable()
export class DefaultProviderRepository extends MongooseRepository<Provider> implements ProviderRepository {
    constructor() {
        super()
        this.model = ProviderModel
    }

    findByCode(code: string): Promise<Provider> {
        return this.findOneBy("code", code)
    }
}