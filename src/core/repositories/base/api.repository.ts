import {RepositoryError} from "../../exceptions/repository.error";
import {injectable} from "inversify";
import Axios, {AxiosError, AxiosInstance, AxiosRequestConfig, AxiosStatic, Method} from "axios";
import Di from "@di/index";
import Logger, {LoggerType} from "@generics/logger";

@injectable()
export default abstract class ApiRepository {
    protected client: AxiosInstance
    protected logger: Logger

    protected baseUrl: string

    constructor() {
        this.client = Axios.create()
        this.logger = Di.getContainer().get<Logger>(LoggerType)
    }

    protected buildUrl(path: string) {
        return this.baseUrl + path
    }

    protected async get<T>(path: string): Promise<any> {
        return this.request<T>("GET", path)
    }

    protected async post<T>(path: string, data: any): Promise<any> {
        return this.request<T>("POST", path, data)
    }

    protected async request<T>(method: Method, path: string, data?: any, axiosOptions?: AxiosRequestConfig): Promise<any> {
        const url = this.buildUrl(path)
        this.logger.debug(`[OUT] ${method} ${url} \n${JSON.stringify(data, null, 4)}\n`)
        return this.client.request<T>({
            method,
            url,
            data,
            ...axiosOptions,
        })
            .then(res => res.data)
            .catch(e => {
                throw ApiRepository.toRepositoryError(e)
            })
    }

    protected static toRepositoryError(e: AxiosError): RepositoryError {
        return new RepositoryError(e.response.status, e.response.data)
    }
}