import {injectable} from "inversify";
import mongoose from "mongoose";
import {Document, FilterQuery, Model} from "mongoose";
import {HttpError} from "../../exceptions/http.error";
import ReadInterface from "@repositories/base/read.interface";
import WriteInterface from "@repositories/base/write.interface";
import {BAD_REQUEST, INTERNAL_SERVER_ERROR, NOT_FOUND} from "http-status-codes";

@injectable()
export default abstract class MongooseRepository<T extends Document> implements ReadInterface<T>, WriteInterface<T> {
    protected model: Model<T>

    async findAll(): Promise<T[]> {
        return this.model.find()
    }

    async findOneById(id: any): Promise<T> {
        return this.findOneBy("_id", id)
    }

    async findOneBy(field: string, value: any): Promise<T> {
        const model = await this.model.findOne(
            this.getConditions({[field]: value})
        )
        if (!model) {
            throw new HttpError(NOT_FOUND, `${this.model.modelName} [${field}:${value}] not found`)
        }
        return model
    }

    async save(item: T): Promise<T> {
        try {
            return await item.save()
        } catch (e) {
            const error = new HttpError(INTERNAL_SERVER_ERROR, e.message)
            if (e instanceof mongoose.Error.ValidationError) {
                error.setStatus(BAD_REQUEST)
            }
            throw error
        }
    }

    async update(id: string, item: T): Promise<T> {
        return this.model.updateOne(
            this.getConditions({"_id": id}),
            item
        )
    }

    async delete(id: string): Promise<T> {
        return this.model.findByIdAndRemove(id)
    }

    protected getConditions(conditions: object): FilterQuery<T> {
        return conditions as FilterQuery<T>
    }
}