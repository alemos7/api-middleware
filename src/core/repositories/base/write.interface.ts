export default interface WriteInterface<T> {
    save(item: T): Promise<T>;
    update(id: string, item: T): Promise<T>;
    delete(id: string): Promise<T>;
}