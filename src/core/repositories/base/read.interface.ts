export default interface ReadInterface<T> {
    findAll(): Promise<T[]>
    findOneById(id: any): Promise<T>;
    findOneBy(field: string, value: any): Promise<T>;
}