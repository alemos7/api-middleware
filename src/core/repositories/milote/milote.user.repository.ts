import {MiloteUserLoginResponse} from "@dto/milote/response/user.login";
import {MiloteUserResponse} from "@dto/milote/response/user";

export interface MiloteUserRepository {
    login(payload: { username: string, password: string }): Promise<MiloteUserLoginResponse>;

    register(payload: any): Promise<MiloteUserResponse>;

    activate(token: string, salt: string, data: any): Promise<any>;

    resetPasswordRequest(data: any): Promise<any>;

    verify(payload: any): Promise<any>;
}