import {MiloteUserRepository} from "@repositories/milote/milote.user.repository";
import {injectable} from "inversify";
import ApiRepository from "@repositories/base/api.repository";
import Axios from "axios";
import {MiloteUserLoginResponse} from "@dto/milote/response/user.login";
import {MiloteUserResponse} from "@dto/milote/response/user";

enum PathMapping {
    Info = "/auth",
    Register = "/auth/register",
    Login = "/auth/login",
    Activate = "/auth/activate/",
    Verify = "/auth/verify",
    PasswordResetRequest = "/auth/password_reset/",
}

@injectable()
export class DefaultMiloteUserRepository extends ApiRepository implements MiloteUserRepository {
    baseUrl = process.env.MILOTE_API_URI || "https://api.milote.com.ar"
    private readonly loginUrl = process.env.MILOTE_LOGIN_URL || "http://sso.milote.com.ar/login"

    public async info(payload: { username: string, password: string }): Promise<any> {
        return this.request("GET", PathMapping.Info, null, {
            auth: payload
        })
    }

    public async login(payload: { username: string, password: string }): Promise<MiloteUserLoginResponse> {
        return Axios.all([
            this.post(PathMapping.Login, payload),
            this.info(payload)
        ]).then(
            Axios.spread(
                (token: object, info: object) => ({...info, ...token} as MiloteUserLoginResponse)
            )
        ).catch(e => {
            if (e.status && e.status === 400) {
                e.redirect_url = this.loginUrl
            }
            throw e
        })
    }

    public async register(payload: any): Promise<MiloteUserResponse> {
        payload.origin = 'SSO'
        return this.post<MiloteUserResponse>(PathMapping.Register, payload)
    }

    public async activate(token: string, salt: string, data: any): Promise<any> {
        return this.post(`${PathMapping.Activate}${token}/${salt}`, data)
    }

    public async resetPasswordRequest(data: any): Promise<any> {
        return this.post(PathMapping.PasswordResetRequest, data)
    }

    public async verify(payload: any): Promise<any> {
        return this.post(PathMapping.Verify, payload)
    }
}