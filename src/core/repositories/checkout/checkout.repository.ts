import {ConfigPayment} from "@dto/checkout/config.payment";
import {ConfigPaymentResult} from "@dto/checkout/config.payment.result";
import {CheckoutOrder} from "@dto/checkout/order";
import {Provider} from "@entities/provider/model";
import {MiloteUserResponse} from "@dto/milote/response/user";

export const CheckoutRepositoryType = Symbol("CheckoutRepository")

export interface CheckoutRepository {
    getLatestOrder(filterkey: string, filterValue: string): Promise<CheckoutOrder>

    createConfigPayment(config: ConfigPayment): Promise<ConfigPaymentResult>

    createConfigPaymentByUserProvider(user: MiloteUserResponse, redirectUrl: string, provider: Provider): Promise<ConfigPaymentResult>
}