import {injectable} from "inversify";
import ApiRepository from "@repositories/base/api.repository";
import {CheckoutRepository} from "@repositories/checkout/checkout.repository";
import {ConfigPayment} from "@dto/checkout/config.payment";
import {ConfigPaymentResult} from "@dto/checkout/config.payment.result";
import {CheckoutOrder} from "@dto/checkout/order";
import {MiloteUserLoginResponse} from "@dto/milote/response/user.login";
import {Provider} from "@entities/provider/model";
import * as https from "https";

enum PathMapping {
    ConfigPayment = "/api/config/payment",
    OrderFilter = "/api/oms/order/filter/{k}/{v}"
}

@injectable()
export class DefaultCheckoutRepository extends ApiRepository implements CheckoutRepository {
    baseUrl = process.env.CHECKOUT_API_URI || "https://api.checkout.milote.com.ar"

    constructor() {
        super();
        this.client.defaults.auth = {
            username: 'milote',
            password: 'milote',
        }
    }

    createConfigPayment(config: ConfigPayment): Promise<ConfigPaymentResult> {
        return this.post<ConfigPaymentResult>(PathMapping.ConfigPayment, config).then(res => res.data)
    }

    createConfigPaymentByUserProvider(user: MiloteUserLoginResponse, redirectUrl: string, provider: Provider) {
        return this.createConfigPayment({
            config: {
                customer: {
                    id: user.id,
                    full_name: `${user.firstname} ${user.lastname}`,
                    document: user.id.toString(),
                    email: user.user.email,
                    address: "address",
                    region: "region"
                },
                items: provider.data.items || [{price: 200, name: "test"}],
                id_lote: user.id,
                description: provider.data.description || "test",
                payment_type: "simple",
                total: provider.data.total || 200,
                callback_success_url: redirectUrl,
                callback_error_url: redirectUrl,
            }
        })
    }

    async getLatestOrder(filterkey: string, filterValue: string): Promise<CheckoutOrder> {
        try {
            const checkoutOrders = await this.get<CheckoutOrder[]>(
                PathMapping.OrderFilter
                    .replace("{k}", filterkey)
                    .replace("{v}", filterValue)
            ).then(res => res.data)
            return checkoutOrders[0]
        } catch (e) {
            return null
        }
    }
}