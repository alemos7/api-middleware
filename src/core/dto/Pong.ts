export class Pong {
    status: string;

    constructor(status: string) {
        this.status = status;
    }

    public getStatus(): string {
        return this.status;
    }
}