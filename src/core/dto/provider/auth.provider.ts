import {Provider} from "@entities/provider/model";

export class AuthProvider {
    private token: string;
    private provider: Provider;

    constructor(token: string, provider: Provider) {
        this.token = token;
        this.provider = provider;
    }

    public getToken(): string {
        return this.token;
    }

    public setToken(value: string) {
        this.token = value;
    }

    public getProvider(): Provider {
        return this.provider;
    }

    public setProvider(value: Provider) {
        this.provider = value;
    }
}