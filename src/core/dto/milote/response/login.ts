export interface MiloteLoginResponse {
    accepted_terms: boolean
    token: string
    notifications: string[]
}