import {MiloteLoginResponse} from "@dto/milote/response/login";
import {MiloteUserResponse} from "@dto/milote/response/user";

export interface MiloteUserLoginResponse extends MiloteLoginResponse, MiloteUserResponse {

}