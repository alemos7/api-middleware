export interface MiloteUserResponse {
    id: number,
    user: { email: string, username: string, password: string },
    created: string,
    firstname: string,
    lastname: string,
    is_over_18: boolean,
    accepted_terms: boolean,
    notifications: string[]
}