export interface ConfigPayment {
    config: {
        customer: {
            id: number,
            "full_name": string
            "document": string
            "email": string
            "address": string
            "region": string
        }
        items: {
            name: string
            price: number
        }[]
        id_lote: number
        description: string
        payment_type: string
        total: number
        callback_success_url: string
        callback_error_url: string
    }
}