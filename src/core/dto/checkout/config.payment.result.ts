import {ConfigPayment} from "@dto/checkout/config.payment";

export interface ConfigPaymentResult extends ConfigPayment {
    id: string,
    route: string,
    created_at: {
        $date: {
            $numberLong: string
        }
    }
}