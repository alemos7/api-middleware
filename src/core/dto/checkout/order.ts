export enum CheckoutOrderStatus {
    accredited = 'accredited'
}

export interface CheckoutOrder {
    id: number,
    status: CheckoutOrderStatus,
    created_at: {
        date: string
    }
}