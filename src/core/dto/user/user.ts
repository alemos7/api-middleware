export class User {
    private email: string;
    private password: string;
    private fiscalId: string;
    private firstname: string;
    private lastname: string;
    private isOver18: boolean;
    private acceptedTerms: boolean;

    public getEmail(): string {
        return this.email;
    }

    public setEmail(value: string): User {
        this.email = value;
        return this;
    }

    public getPassword(): string {
        return this.password;
    }

    public setPassword(value: string): User {
        this.password = value;
        return this;
    }

    public getFiscalId(): string {
        return this.fiscalId;
    }

    public setFiscalId(value: string): User {
        this.fiscalId = value;
        return this;
    }

    public getFirstname(): string {
        return this.firstname;
    }

    public setFirstname(value: string): User {
        this.firstname = value;
        return this;
    }

    public getLastname(): string {
        return this.lastname;
    }

    public setLastname(value: string) {
        this.lastname = value;
        return this;
    }

    public getIsOver18(): boolean {
        return this.isOver18;
    }

    public setIsOver18(value: boolean) {
        this.isOver18 = value;
    }

    public getAcceptedTerms(): boolean {
        return this.acceptedTerms;
    }

    public setAcceptedTerms(value: boolean) {
        this.acceptedTerms = value;
    }
}