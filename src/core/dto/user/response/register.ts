import {MiloteUserResponse} from "@dto/milote/response/user";

export interface UserRegisterResponse {
    user: MiloteUserResponse,
    access: boolean,
    redirect_url: string,
}