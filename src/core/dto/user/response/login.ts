import {MiloteUserLoginResponse} from "@dto/milote/response/user.login";

export interface UserLoginResponse {
    user: MiloteUserLoginResponse,
    access: boolean,
    redirect_url: string,
}