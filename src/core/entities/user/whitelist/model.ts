import mongoose, {Document} from 'mongoose';
import {UserWhitelistSchema} from "@entities/user/whitelist/schema";

export interface UserWhitelist extends Document {
    provider: string;
    email: string;
}

export default mongoose.model<UserWhitelist>('User_Whitelist', UserWhitelistSchema);