import {Schema} from 'mongoose';

const UserWhitelistSchema: Schema = new Schema({
    provider: {
        type: String,
        required: true
    },
    email: {
        type: String,
        required: true,
        lowercase: true
    }
}, {versionKey: false});

export {UserWhitelistSchema};