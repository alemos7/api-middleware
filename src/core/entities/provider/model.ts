import mongoose, {Document} from 'mongoose';
import {ProviderSchema} from "@entities/provider/schema";

export interface Provider extends Document {
    code: string;
    email: string;
    password: string;
    data: any;
}

export default mongoose.model<Provider>('Provider', ProviderSchema);