import {Schema} from 'mongoose';

const ProviderSchema: Schema = new Schema({
    code: {
        type: String,
        required: true,
        unique: true
    },
    email: {
        type: String,
        unique: true,
        required: true,
        lowercase: true
    },
    password: {
        type: String,
        required: true
    },
    data: {
        type: Schema.Types.Mixed,
        required: false
    }
}, {versionKey: false});

export {ProviderSchema};