import "reflect-metadata";
import 'module-alias/register';
import di from "@di/index";
import {Ping} from "@useCase/tools/ping";
import {DefaultPing} from "@useCase/tools/implementation/default.ping";
import {SaveProvider} from "@useCase/provider/save.provider";
import {DefaultSaveProvider} from "@useCase/provider/impl/default.save.provider";
import {ProviderFactory} from "@factories/provider/provider.factory";
import {DefaultProviderFactory} from "@factories/provider/impl/default.provider.factory";
import {DefaultProviderRepository} from "@repositories/provider/impl/default.provider.repository";
import {RegisterProvider} from "@useCase/provider/register.provider";
import {DefaultRegisterProvider} from "@useCase/provider/impl/default.register.provider";
import {EncryptProviderPassword} from "@useCase/provider/encrypt.provider.password";
import {DefaultEncryptProviderPassword} from "@useCase/provider/impl/default.encrypt.provider.password";
import {AuthProviderFactory} from "@factories/provider/auth.provider.factory";
import {DefaultAuthProviderFactory} from "@factories/provider/impl/default.auth.provider.factory";
import {RetrieveTokenForProvider} from "@useCase/provider/retrieve.token.for.provider";
import {DefaultRetrieveTokenForProvider} from "@useCase/provider/impl/default.retrieve.token.for.provider";
import {DefaultMiloteUserRepository} from "@repositories/milote/impl/default.milote.user.repository";
import {UserFactory} from "@factories/dto/user/user.factory";
import {DefaultUserFactory} from "@factories/dto/user/impl/default.user.factory";
import {UserRegister, UserRegisterType} from "@useCase/user/user.register";
import {DefaultUserRegister} from "@useCase/user/impl/default.user.register";
import {MiloteUserRepository} from "@repositories/milote/milote.user.repository";
import {ProviderRepository, ProviderRepositoryType} from "@repositories/provider/provider.repository";
import {RetrieveProviders, RetrieveProvidersType} from "@useCase/provider/retrieve.providers";
import {DefaultRetrieveProviders} from "@useCase/provider/impl/default.retrieve.providers";
import {UserLogin, UserLoginType} from "@useCase/user/user.login";
import {DefaultUserLogin} from "@useCase/user/impl/default.user.login";
import {LoginProvider, LoginProviderType} from "@useCase/provider/login.provider";
import {DefaultLoginProvider} from "@useCase/provider/impl/default.login.provider";
import {CompareProviderPassword, CompareProviderPasswordType} from "@useCase/provider/compare.provider.password";
import {DefaultCompareProviderPassword} from "@useCase/provider/impl/default.compare.provider.password";
import {UserTokenActivate, UserTokenActivateType} from "@useCase/user/user.token.activate";
import {DefaultUserTokenActivate} from "@useCase/user/impl/default.user.token.activate";
import {UserResetPassword, UserResetPasswordType} from "@useCase/user/user.reset.password";
import {DefaultUserResetPassword} from "@useCase/user/impl/default.user.reset.password";
import {RetrieveProviderByCode, RetrieveProviderByCodeType} from "@useCase/provider/retrieve.provider.by.code";
import {DefaultRetrieveProviderByCode} from "@useCase/provider/impl/default.retrieve.provider.by.code";
import {
    UserWhitelistRepository,
    UserWhitelistRepositoryType
} from "@repositories/user/whitelist/user.whitelist.repository";
import {DefaultUserWhitelistRepository} from "@repositories/user/whitelist/impl/default.user.whitelist.repository";
import {UserWhitelistCreate, UserWhitelistCreateType} from "@useCase/user/whitelist/user.whitelist.create";
import {DefaultUserWhitelistCreate} from "@useCase/user/whitelist/impl/default.user.whitelist.create";
import {UserWhitelistExists, UserWhiteListExistsType} from "@useCase/user/whitelist/user.whitelist.exists";
import {DefaultUserWhitelistExists} from "@useCase/user/whitelist/impl/default.user.whitelist.exists";
import {UserWhitelistFactory, UserWhitelistFactoryType} from "@factories/user/whitelist/user.whitelist.factory";
import {DefaultUserWhitelistFactory} from "@factories/user/whitelist/impl/default.user.whitelist.factory";
import {CheckoutRepository, CheckoutRepositoryType} from "@repositories/checkout/checkout.repository";
import {DefaultCheckoutRepository} from "@repositories/checkout/impl/default.checkout.repository";
import {UserTokenValidate, UserTokenValidateType} from "@useCase/user/user.token.validate";
import {DefaultUserTokenValidate} from "@useCase/user/impl/default.user.token.validate";
import {UserWhitelistList, UserWhiteListListType} from "@useCase/user/whitelist/user.whitelist.list";
import {DefaultUserWhitelistList} from "@useCase/user/whitelist/impl/default.user.whitelist.list";
import {UserWhitelistDelete, UserWhitelistDeleteType} from "@useCase/user/whitelist/use.whitelist.delete";
import {DefaultUserWhitelistDelete} from "@useCase/user/whitelist/impl/default.user.whitelist.delete";

const bindDefaults = () => {
    const diContainer = di.getContainer();
    /////////////////////////////////////////////////////////
    // ******************* USE CASES ************************
    /////////////////////////////////////////////////////////
    diContainer.bind<Ping>("Ping").to(DefaultPing).inSingletonScope();
    // PROVIDER
    diContainer.bind<RetrieveProviders>(RetrieveProvidersType).to(DefaultRetrieveProviders).inSingletonScope();
    diContainer.bind<RetrieveProviderByCode>(RetrieveProviderByCodeType).to(DefaultRetrieveProviderByCode).inSingletonScope();
    diContainer.bind<SaveProvider>("SaveProvider").to(DefaultSaveProvider).inSingletonScope();
    diContainer.bind<RegisterProvider>("RegisterProvider").to(DefaultRegisterProvider).inSingletonScope();
    diContainer.bind<EncryptProviderPassword>("EncryptProviderPassword").to(DefaultEncryptProviderPassword).inSingletonScope();
    diContainer.bind<CompareProviderPassword>(CompareProviderPasswordType).to(DefaultCompareProviderPassword).inSingletonScope();
    diContainer.bind<RetrieveTokenForProvider>("RetrieveTokenForProvider").to(DefaultRetrieveTokenForProvider).inSingletonScope();
    diContainer.bind<LoginProvider>(LoginProviderType).to(DefaultLoginProvider).inSingletonScope();
    // USER
    diContainer.bind<UserRegister>(UserRegisterType).to(DefaultUserRegister).inSingletonScope();
    diContainer.bind<UserLogin>(UserLoginType).to(DefaultUserLogin).inSingletonScope();
    diContainer.bind<UserTokenActivate>(UserTokenActivateType).to(DefaultUserTokenActivate).inSingletonScope();
    diContainer.bind<UserTokenValidate>(UserTokenValidateType).to(DefaultUserTokenValidate).inSingletonScope();
    diContainer.bind<UserResetPassword>(UserResetPasswordType).to(DefaultUserResetPassword).inSingletonScope();
    // USER WHITELIST
    diContainer.bind<UserWhitelistCreate>(UserWhitelistCreateType).to(DefaultUserWhitelistCreate).inSingletonScope();
    diContainer.bind<UserWhitelistExists>(UserWhiteListExistsType).to(DefaultUserWhitelistExists).inSingletonScope();
    diContainer.bind<UserWhitelistList>(UserWhiteListListType).to(DefaultUserWhitelistList).inSingletonScope();
    diContainer.bind<UserWhitelistDelete>(UserWhitelistDeleteType).to(DefaultUserWhitelistDelete).inSingletonScope();

    /////////////////////////////////////////////////////////
    // ******************* REPOSITORIES *********************
    /////////////////////////////////////////////////////////
    diContainer.bind<MiloteUserRepository>("MiloteUserRepository").to(DefaultMiloteUserRepository);
    diContainer.bind<ProviderRepository>(ProviderRepositoryType).to(DefaultProviderRepository);
    diContainer.bind<UserWhitelistRepository>(UserWhitelistRepositoryType).to(DefaultUserWhitelistRepository);
    diContainer.bind<CheckoutRepository>(CheckoutRepositoryType).to(DefaultCheckoutRepository);

    /////////////////////////////////////////////////////////
    // ******************* FACTORIES ************************
    /////////////////////////////////////////////////////////
    diContainer.bind<ProviderFactory>("ProviderFactory").to(DefaultProviderFactory);
    diContainer.bind<AuthProviderFactory>("AuthProviderFactory").to(DefaultAuthProviderFactory);
    diContainer.bind<UserFactory>("UserFactory").to(DefaultUserFactory);
    diContainer.bind<UserWhitelistFactory>(UserWhitelistFactoryType).to(DefaultUserWhitelistFactory);
};

export {bindDefaults}