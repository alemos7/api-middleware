import {Request, Response, NextFunction} from "express";
import {Provider} from "@entities/provider/model";
import di from "@di/index";
import {ProviderRepository, ProviderRepositoryType} from "@repositories/provider/provider.repository";
import Logger, {LoggerType} from "@generics/logger";
import {validateJwtMiddleware} from "@generics/jwt/validateJwtMiddleware";
import {HttpError} from "../../exceptions/http.error";
import {UNAUTHORIZED} from "http-status-codes";

const unauthorizedError = new HttpError(UNAUTHORIZED, "Unauthorized")

const allowSelfProvider = async (req: Request, res: Response, next: NextFunction) => {
    const container = di.getContainer();
    const logger = container.get<Logger>(LoggerType);
    const {provider} = res.locals
    if (!provider) {
        return res.status(unauthorizedError.status).json(unauthorizedError);
    }

    if (provider.code !== process.env.SELF_PROVIDER_CODE) {
        logger.warn(`Provider '${provider.code}' is trying to access to a protected resource ${req.path}`);
        return res.status(unauthorizedError.status).json(unauthorizedError);
    }
    next()
}

export const setSelfProvider = async (req: Request, res: Response, next: NextFunction) => {
    const container = di.getContainer();
    const logger = container.get<Logger>(LoggerType);
    const providerRepository = container.get<ProviderRepository>(ProviderRepositoryType);
    const providerCode = process.env.SELF_PROVIDER_CODE;
    let provider: Provider;
    try {
        provider = await providerRepository.findByCode(providerCode)
    } catch (e) {
        logger.error(`loadProviderMiddleware: token is valid but provider with code: ${providerCode} not found`)
        return res.status(unauthorizedError.status).json(unauthorizedError);
    }
    res.locals.provider = provider
    next()
}

const loadProviderMiddleware = async (req: Request, res: Response, next: NextFunction) => {
    const container = di.getContainer();
    const providerRepository = container.get<ProviderRepository>(ProviderRepositoryType);
    const logger = container.get<Logger>(LoggerType);
    const providerId = res.locals.jwtPayload.uid;
    let provider: Provider;
    try {
        provider = await providerRepository.findOneById(providerId)
    } catch (e) {
        logger.error(`loadProviderMiddleware: token is valid but provider with id: ${providerId} not found`)
        return res.status(unauthorizedError.status).json(unauthorizedError);
    }

    res.locals.provider = provider
    next()
}

export const validateLoggedProvider = [validateJwtMiddleware, loadProviderMiddleware]

export const validateSelfProvider = [...validateLoggedProvider, allowSelfProvider]