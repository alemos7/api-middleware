import {HttpError} from "../../exceptions/http.error";
import {INTERNAL_SERVER_ERROR} from "http-status-codes";
import di from "@di/index";
import Logger, {LoggerType} from "@generics/logger";
import {isDevelopment} from "@generics/environment";

export const jsonErrorHandling = (err: any, req: any, res: any) => {
    const container = di.getContainer();
    const logger = container.get<Logger>(LoggerType);
    if (isDevelopment()) {
        logger.error(err.stack)
    }
    let error = err
    if (!(error instanceof HttpError)) {
        error = new HttpError(err.statusCode || INTERNAL_SERVER_ERROR, err.message)
    }
    res.status(error.status).json(error);
}
