import {Provider} from "@entities/provider/model";

export const RetrieveProviderByCodeType = Symbol("RetrieveProviderByCode")

export interface RetrieveProviderByCode {
    get(code: string): Promise<Provider>;
}