import {AuthProvider} from "@dto/provider/auth.provider";

export interface RegisterProvider {
    execute(data: any): Promise<AuthProvider>;
}