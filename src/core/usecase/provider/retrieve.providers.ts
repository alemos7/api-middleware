import {Provider} from "@entities/provider/model"

export const RetrieveProvidersType = Symbol("RetrieveProviders")

export interface RetrieveProviders {
    get(): Promise<Provider[]>;
}