import {Provider} from "@entities/provider/model"

export interface SaveProvider {
    execute(data: any): Promise<Provider>;
}