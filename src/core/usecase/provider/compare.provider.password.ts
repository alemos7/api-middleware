export const CompareProviderPasswordType = Symbol('CompareProviderPassword')

export interface CompareProviderPassword {
    execute(password: string, hash: string): Promise<boolean>;
}