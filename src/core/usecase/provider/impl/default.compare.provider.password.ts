import bcrypt from "bcryptjs";
import {injectable} from "inversify";
import {CompareProviderPassword} from "@useCase/provider/compare.provider.password";

@injectable()
export class DefaultCompareProviderPassword implements CompareProviderPassword {
    async execute(password: string, hash: string): Promise<boolean> {
        return bcrypt.compare(password, hash);
    }
}
