import bcrypt from "bcryptjs";
import {EncryptProviderPassword} from "@useCase/provider/encrypt.provider.password";
import {injectable} from "inversify";

@injectable()
export class DefaultEncryptProviderPassword implements EncryptProviderPassword {
    async execute(password: string): Promise<string> {
        const salt = await bcrypt.genSalt(10);
        return bcrypt.hash(password, salt);
    }
}