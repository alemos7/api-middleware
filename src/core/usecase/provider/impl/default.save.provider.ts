import {SaveProvider} from "@useCase/provider/save.provider";
import {Provider} from "@entities/provider/model";
import {ProviderFactory} from "@factories/provider/provider.factory";
import {inject, injectable} from "inversify";
import {ProviderRepository, ProviderRepositoryType} from "@repositories/provider/provider.repository";

@injectable()
export class DefaultSaveProvider implements SaveProvider {
    constructor(
        @inject("ProviderFactory") private providerFactory: ProviderFactory,
        @inject(ProviderRepositoryType) private providerRepository: ProviderRepository,
    ) {
    }

    execute(data: any): Promise<Provider> {
        return this.providerRepository.save(this.providerFactory.create(data));
    }
}