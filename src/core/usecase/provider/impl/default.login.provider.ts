import HttpStatus from 'http-status-codes';
import {inject, injectable} from "inversify";
import {AuthProvider} from "@dto/provider/auth.provider";
import {AuthProviderFactory} from "@factories/provider/auth.provider.factory";
import {RetrieveTokenForProvider} from "@useCase/provider/retrieve.token.for.provider";
import {ProviderRepository, ProviderRepositoryType} from "@repositories/provider/provider.repository";
import {LoginProvider} from "@useCase/provider/login.provider";
import {CompareProviderPassword, CompareProviderPasswordType} from "@useCase/provider/compare.provider.password";
import {Provider} from "@entities/provider/model";
import {HttpError} from "../../../exceptions/http.error";

@injectable()
export class DefaultLoginProvider implements LoginProvider {
    constructor(
        @inject(ProviderRepositoryType) private providerRepository: ProviderRepository,
        @inject(CompareProviderPasswordType) private compareProviderPassword: CompareProviderPassword,
        @inject("AuthProviderFactory") private authProviderFactory: AuthProviderFactory,
        @inject("RetrieveTokenForProvider") private retrieveTokenForProvider: RetrieveTokenForProvider
    ) {
    }

    async execute(requestBody: any): Promise<AuthProvider> {
        const genericPasswordError = 'invalid provider or password'
        const {code, password} = requestBody
        if (!code || !password) {
            throw new HttpError(HttpStatus.FORBIDDEN, "code and password are required fields")
        }
        let provider: Provider
        try {
            provider = await this.providerRepository.findByCode(code)
        } catch (e) {
            throw new HttpError(HttpStatus.FORBIDDEN, genericPasswordError)
        }
        const isValidPassword = await this.compareProviderPassword.execute(password, provider.password)
        if (!isValidPassword) {
            throw new HttpError(HttpStatus.FORBIDDEN, genericPasswordError)
        }
        const jwtToken = this.retrieveTokenForProvider.get(provider._id)
        return this.authProviderFactory.create(jwtToken, provider);
    }
}