import {Provider} from "@entities/provider/model";
import {inject, injectable} from "inversify";
import {ProviderRepository, ProviderRepositoryType} from "@repositories/provider/provider.repository";
import {RetrieveProviderByCode} from "@useCase/provider/retrieve.provider.by.code";

@injectable()
export class DefaultRetrieveProviderByCode implements RetrieveProviderByCode {
    constructor(@inject(ProviderRepositoryType) private providerRepository: ProviderRepository) {
    }

    async get(code: string): Promise<Provider> {
        return await this.providerRepository.findByCode(code)
    }
}