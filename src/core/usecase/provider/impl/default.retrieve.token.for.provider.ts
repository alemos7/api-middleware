import {RetrieveTokenForProvider} from "@useCase/provider/retrieve.token.for.provider";
import {injectable} from "inversify";
import {createJwt} from "@generics/jwt/create.jwt";

@injectable()
export class DefaultRetrieveTokenForProvider implements RetrieveTokenForProvider {
    get(providerId: object): string {
        return createJwt({
            uid: providerId
        })
    }
}