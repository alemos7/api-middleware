import {Provider} from "@entities/provider/model";
import {inject, injectable} from "inversify";
import {RetrieveProviders} from "@useCase/provider/retrieve.providers";
import {ProviderRepository, ProviderRepositoryType} from "@repositories/provider/provider.repository";

@injectable()
export class DefaultRetrieveProviders implements RetrieveProviders {
    constructor(@inject(ProviderRepositoryType) private providerRepository: ProviderRepository) {
    }

    async get(): Promise<Provider[]> {
        return this.providerRepository.findAll()
    }
}