import {SaveProvider} from "@useCase/provider/save.provider";
import {ProviderFactory} from "@factories/provider/provider.factory";
import {inject, injectable} from "inversify";
import {RegisterProvider} from "@useCase/provider/register.provider";
import {AuthProvider} from "@dto/provider/auth.provider";
import {Provider} from "@entities/provider/model";
import {EncryptProviderPassword} from "@useCase/provider/encrypt.provider.password";
import {AuthProviderFactory} from "@factories/provider/auth.provider.factory";
import {RetrieveTokenForProvider} from "@useCase/provider/retrieve.token.for.provider";

@injectable()
export class DefaultRegisterProvider implements RegisterProvider {

    private providerFactory: ProviderFactory;
    private saveProvider: SaveProvider;
    private encryptProviderPassword: EncryptProviderPassword;
    private authProviderFactory: AuthProviderFactory;
    private retrieveTokenForProvider: RetrieveTokenForProvider;

    constructor(
        @inject("ProviderFactory") providerFactory: ProviderFactory,
        @inject("SaveProvider") saveProvider: SaveProvider,
        @inject("EncryptProviderPassword") encryptProviderPassword: EncryptProviderPassword,
        @inject("AuthProviderFactory") authProviderFactory: AuthProviderFactory,
        @inject("RetrieveTokenForProvider") retrieveTokenForProvider: RetrieveTokenForProvider
    ) {
        this.providerFactory = providerFactory;
        this.saveProvider = saveProvider;
        this.encryptProviderPassword = encryptProviderPassword;
        this.authProviderFactory = authProviderFactory;
        this.retrieveTokenForProvider = retrieveTokenForProvider
    }

    async execute(data: any): Promise<AuthProvider> {
        const provider: Provider = this.providerFactory.create(data);
        provider.password = await this.encryptProviderPassword.execute(provider.password);
        const providerPersisted = await this.saveProvider.execute(provider);
        const jwtToken = this.retrieveTokenForProvider.get(providerPersisted._id)
        return this.authProviderFactory.create(jwtToken, provider);
    }
}