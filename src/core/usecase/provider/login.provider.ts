import {AuthProvider} from "@dto/provider/auth.provider";

export const LoginProviderType = Symbol("LoginProvider")

export interface LoginProvider {
    execute(requestBody: any): Promise<AuthProvider>;
}