export interface EncryptProviderPassword {
    execute(data: string): Promise<string>;
}