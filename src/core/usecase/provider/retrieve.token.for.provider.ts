export interface RetrieveTokenForProvider {
    get(providerId: object): string;
}