import {Pong} from "@dto/Pong";

export interface Ping {
    get(): Pong
}