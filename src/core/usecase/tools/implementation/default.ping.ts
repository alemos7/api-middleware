import {injectable} from "inversify";
import "reflect-metadata";
import {Pong} from "@dto/Pong";
import {Ping} from "@useCase/tools/ping";

@injectable()
class DefaultPing implements Ping {
    get(): Pong {
        return new Pong("pong");
    }
}

export {DefaultPing}