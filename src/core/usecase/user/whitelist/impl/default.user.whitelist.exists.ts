import {UserWhitelistExists} from "@useCase/user/whitelist/user.whitelist.exists";
import {inject, injectable} from "inversify";
import {
    UserWhitelistRepository,
    UserWhitelistRepositoryType
} from "@repositories/user/whitelist/user.whitelist.repository";

@injectable()
export class DefaultUserWhitelistExists implements UserWhitelistExists {
    constructor(@inject(UserWhitelistRepositoryType) private userWhitelistRepository: UserWhitelistRepository) {
    }

    async get(email: string, providerCode: string): Promise<{ exists: boolean }> {
        return {
            exists: await this.userWhitelistRepository.exists(email, providerCode)
        }
    }
}