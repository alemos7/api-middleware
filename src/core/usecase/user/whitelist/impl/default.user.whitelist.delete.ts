import {UserWhitelistCreate} from "@useCase/user/whitelist/user.whitelist.create";
import {inject, injectable} from "inversify";
import {
    UserWhitelistRepository,
    UserWhitelistRepositoryType
} from "@repositories/user/whitelist/user.whitelist.repository";
import {UserWhitelistFactory, UserWhitelistFactoryType} from "@factories/user/whitelist/user.whitelist.factory";
import {HttpError} from "../../../../exceptions/http.error";
import {BAD_REQUEST} from "http-status-codes";
import {ProviderRepository, ProviderRepositoryType} from "@repositories/provider/provider.repository";
import {UserWhitelistDelete} from "@useCase/user/whitelist/use.whitelist.delete";

@injectable()
export class DefaultUserWhitelistDelete implements UserWhitelistDelete {
    constructor(
        @inject(UserWhitelistRepositoryType) private userWhitelistRepository: UserWhitelistRepository,
        @inject(ProviderRepositoryType) private providerRepository: ProviderRepository,
        @inject(UserWhitelistFactoryType) private userWhitelistFactory: UserWhitelistFactory,
    ) {
    }

    async execute(email: string, providerCode: string): Promise<boolean> {
        const whitelist = this.userWhitelistFactory.create({
            email,
            provider: providerCode,
        });
        await this.providerRepository.findByCode(whitelist.provider); // this validates provider
        return this.userWhitelistRepository.deleteByEmailAndProviderCode(whitelist.email, whitelist.provider)
    }
}