import {UserWhitelistList} from "@useCase/user/whitelist/user.whitelist.list";
import {inject, injectable} from "inversify";
import {
    UserWhitelistRepository,
    UserWhitelistRepositoryType
} from "@repositories/user/whitelist/user.whitelist.repository";
import {UserWhitelist} from "@entities/user/whitelist/model";

@injectable()
export class DefaultUserWhitelistList implements UserWhitelistList {
    constructor(@inject(UserWhitelistRepositoryType) private userWhitelistRepository: UserWhitelistRepository) {
    }

    async get(): Promise<UserWhitelist[]> {
        return this.userWhitelistRepository.findAll()
    }
}