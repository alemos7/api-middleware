import {UserWhitelistCreate} from "@useCase/user/whitelist/user.whitelist.create";
import {inject, injectable} from "inversify";
import {
    UserWhitelistRepository,
    UserWhitelistRepositoryType
} from "@repositories/user/whitelist/user.whitelist.repository";
import {UserWhitelistFactory, UserWhitelistFactoryType} from "@factories/user/whitelist/user.whitelist.factory";
import {HttpError} from "../../../../exceptions/http.error";
import {BAD_REQUEST} from "http-status-codes";
import {ProviderRepository, ProviderRepositoryType} from "@repositories/provider/provider.repository";

@injectable()
export class DefaultUserWhitelistCreate implements UserWhitelistCreate {
    constructor(
        @inject(UserWhitelistRepositoryType) private userWhitelistRepository: UserWhitelistRepository,
        @inject(ProviderRepositoryType) private providerRepository: ProviderRepository,
        @inject(UserWhitelistFactoryType) private userWhitelistFactory: UserWhitelistFactory,
    ) {
    }

    async execute(data: any): Promise<any> {
        const whitelist = this.userWhitelistFactory.create(data);
        await this.providerRepository.findByCode(whitelist.provider); // this validates provider
        if (await this.userWhitelistRepository.exists(whitelist.email, whitelist.provider)) {
            throw new HttpError(
                BAD_REQUEST,
                `${whitelist.email} already exists for provider ${whitelist.provider}`
            )
        }
        return await this.userWhitelistRepository.save(whitelist)
    }
}