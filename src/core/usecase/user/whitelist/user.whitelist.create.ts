export const UserWhitelistCreateType = Symbol("UserWhitelistCreate")

export interface UserWhitelistCreate {
    execute(data: any): Promise<any>;
}