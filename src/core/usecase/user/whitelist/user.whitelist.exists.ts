export const UserWhiteListExistsType = Symbol("UserWhitelistExists")

export interface UserWhitelistExists {
    get(email: string, providerCode: string): Promise<{ exists: boolean }>
}