export const UserWhitelistDeleteType = Symbol("UserWhitelistDelete")

export interface UserWhitelistDelete {
    execute(email: string, providerCode: string): Promise<boolean>;
}