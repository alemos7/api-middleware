import {UserWhitelist} from "@entities/user/whitelist/model";

export const UserWhiteListListType = Symbol("UserWhitelistList")

export interface UserWhitelistList {
    get(): Promise<UserWhitelist[]>
}