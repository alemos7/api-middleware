export const UserResetPasswordType = Symbol('UserResetPassword')

export interface UserResetPassword {
    execute(data: any): Promise<any>;
}