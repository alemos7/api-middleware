import {UserLoginResponse} from "@dto/user/response/login";
import {Provider} from "@entities/provider/model";

export const UserLoginType = Symbol('UserLogin')

export interface UserLogin {
    execute(data: any, provider: Provider): Promise<UserLoginResponse>;
}