export const UserTokenActivateType = Symbol('UserTokenActivate')

export interface UserTokenActivate {
    execute(token: string, salt: string, data: any): Promise<any>;
}