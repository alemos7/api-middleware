import {Provider} from "@entities/provider/model";

export const UserTokenValidateType = Symbol('UserTokenValidate')

export interface UserTokenValidate {
    execute(data: any, provider: Provider): Promise<any>;
}