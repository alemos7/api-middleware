import {inject, injectable} from "inversify";
import {MiloteUserRepository} from "@repositories/milote/milote.user.repository";
import {UserTokenActivate} from "@useCase/user/user.token.activate";
import {HttpError} from "../../../exceptions/http.error";
import {BAD_REQUEST, NOT_FOUND} from "http-status-codes";

@injectable()
export class DefaultUserTokenActivate implements UserTokenActivate {
    constructor(
        @inject("MiloteUserRepository") private miloteUserRepository: MiloteUserRepository,
    ) {
    }

    async execute(token: string, salt: string, data: any): Promise<any> {
        try {
            const res = await this.miloteUserRepository.activate(token, salt, data);
            return Promise.resolve(res)
        } catch (e) {
            // mask milote not-found error
            if (e.status === NOT_FOUND) {
                return Promise.reject(new HttpError(BAD_REQUEST, "Invalid Token"))
            }
            return e
        }
    }
}