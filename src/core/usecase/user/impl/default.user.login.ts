import {injectable} from "inversify";
import {UserLogin} from "@useCase/user/user.login";
import {UserLoginResponse} from "@dto/user/response/login";
import {BaseUserFlow, UserFlowType} from "@useCase/user/impl/base.user.flow";

@injectable()
export class DefaultUserLogin extends BaseUserFlow<UserLoginResponse> implements UserLogin {
    protected requiredFields = ['payload.username', 'payload.password']
    protected flow = UserFlowType.Login
}