import {UserRegister} from "@useCase/user/user.register";
import {injectable} from "inversify";
import {UserRegisterResponse} from "@dto/user/response/register";
import {BaseUserFlow, UserFlowType} from "@useCase/user/impl/base.user.flow";

@injectable()
export class DefaultUserRegister extends BaseUserFlow<UserRegisterResponse> implements UserRegister {
    protected requiredFields = ["payload"]
    protected flow = UserFlowType.Register
}