import {inject, injectable} from "inversify";
import {MiloteUserRepository} from "@repositories/milote/milote.user.repository";
import {UserResetPassword} from "@useCase/user/user.reset.password";

@injectable()
export class DefaultUserResetPassword implements UserResetPassword{
    constructor(
        @inject("MiloteUserRepository") private miloteUserRepository: MiloteUserRepository,
    ) {
    }

    async execute(data: any): Promise<any> {
        return await this.miloteUserRepository.resetPasswordRequest(data);
    }
}