import {inject, injectable} from "inversify";
import {UserFactory} from "@factories/dto/user/user.factory";
import {MiloteUserRepository} from "@repositories/milote/milote.user.repository";
import {UserWhitelistExists, UserWhiteListExistsType} from "@useCase/user/whitelist/user.whitelist.exists";
import {CheckoutRepository, CheckoutRepositoryType} from "@repositories/checkout/checkout.repository";
import dotProp from "dot-prop";
import {HttpError} from "../../../exceptions/http.error";
import {BAD_REQUEST} from "http-status-codes";
import {Provider} from "@entities/provider/model";
import {CheckoutOrderStatus} from "@dto/checkout/order";
import {MiloteUserResponse} from "@dto/milote/response/user";
import {UserLoginResponse} from "@dto/user/response/login";
import {UserRegisterResponse} from "@dto/user/response/register";

export enum UserFlowType {
    Register = 'register',
    Login = 'login',
}

@injectable()
export abstract class BaseUserFlow<T extends UserLoginResponse | UserRegisterResponse> {
    protected requiredFields: string[]
    protected flow: UserFlowType

    constructor(
        @inject("UserFactory") private userFactory: UserFactory,
        @inject("MiloteUserRepository") private miloteUserRepository: MiloteUserRepository,
        @inject(UserWhiteListExistsType) private userWhitelistExists: UserWhitelistExists,
        @inject(CheckoutRepositoryType) private checkoutRepository: CheckoutRepository,
    ) {
        if (this.flow === null) {
            throw new Error("invalid user flow type")
        }
    }

    async execute(data: any, provider: Provider): Promise<T> {
        this.validateRequiredFields(data)
        const res = await this.createResponse(data)

        if (await this.isWhitelisted(res.user, provider) || await this.isAccredited(res.user)) {
            return res
        }

        res.access = false
        res.redirect_url = await this.getPaymentUrl(res.user, data.redirect_url, provider)
        return res
    }

    protected async createResponse(data: any): Promise<T> {
        return {
            user: await this.miloteUserRepository[this.flow](data.payload),
            access: true,
            redirect_url: null
        } as T;
    }

    protected validateRequiredFields(data: object) {
        const fields: string[] = []
        for (const i of this.requiredFields) {
            if (!dotProp.get(data, i, false)) {
                fields.push(i)
            }
        }
        if (fields.length > 0) {
            throw new HttpError(BAD_REQUEST, `required fields: ${fields.join(", ")}`)
        }
    }

    protected async isWhitelisted(user: MiloteUserResponse, provider: Provider): Promise<boolean> {
        const whitelist = await this.userWhitelistExists.get(user.user.email, provider.code)
        return whitelist.exists
    }

    protected async isAccredited(user: MiloteUserResponse) {
        const latestOrder = await this.checkoutRepository.getLatestOrder(
            "customer_email",
            user.user.email
        )
        return latestOrder && latestOrder.status === CheckoutOrderStatus.accredited
    }

    protected async getPaymentUrl(user: MiloteUserResponse, redirectUrl: string, provider: Provider): Promise<string> {
        const configPayment = await this.checkoutRepository
            .createConfigPaymentByUserProvider(user, redirectUrl, provider)
        return configPayment.route
    }
}