import {inject, injectable} from "inversify";
import {MiloteUserRepository} from "@repositories/milote/milote.user.repository";
import {UserTokenValidate} from "@useCase/user/user.token.validate";

@injectable()
export class DefaultUserTokenValidate implements UserTokenValidate {
    constructor(
        @inject("MiloteUserRepository") private miloteUserRepository: MiloteUserRepository,
    ) {
    }

    async execute(data: any): Promise<any> {
        return await this.miloteUserRepository.verify(data);
    }
}