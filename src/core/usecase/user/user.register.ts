import {UserRegisterResponse} from "@dto/user/response/register";
import {Provider} from "@entities/provider/model";

export const UserRegisterType = Symbol('UserRegister')

export interface UserRegister {
    execute(data: any, provider: Provider): Promise<UserRegisterResponse>;
}