export class HttpError extends Error {
    status: number;
    errors: any;

    constructor(status?: number, errors?: any) {
        super()
        this.status = status;
        this.errors = [errors];
    }

    public getStatus(): number {
        return this.status;
    }

    public getErrors(): any {
        return this.errors;
    }

    public setStatus(status: number): HttpError {
        this.status = status;
        return this;
    }

    public setErrors(errors: any): HttpError {
        this.errors = errors;
        return this;
    }
}