import {Provider} from "@entities/provider/model";
import {Factory} from "@factories/factory";

export interface ProviderFactory extends Factory<Provider> {
}