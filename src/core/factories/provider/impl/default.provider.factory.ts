import {ProviderFactory} from "@factories/provider/provider.factory";
import Provider from "@entities/provider/model";
import {injectable} from "inversify";


@injectable()
export class DefaultProviderFactory implements ProviderFactory {
    create(data: any): any {
        return new Provider(data);
    }
}