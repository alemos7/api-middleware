import {injectable} from "inversify";
import {AuthProvider} from "@dto/provider/auth.provider";
import {AuthProviderFactory} from "@factories/provider/auth.provider.factory";


@injectable()
export class DefaultAuthProviderFactory implements AuthProviderFactory {
    create(token: any, data: any): any {
        return new AuthProvider(token, data);
    }
}