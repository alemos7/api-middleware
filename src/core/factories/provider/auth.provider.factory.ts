import {AuthProvider} from "@dto/provider/auth.provider";

export interface AuthProviderFactory {
    create(token: any, provider: any): AuthProvider
}