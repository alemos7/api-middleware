import {UserFactory} from "@factories/dto/user/user.factory";
import {injectable} from "inversify";
import {User} from "@dto/user/user";

@injectable()
export class DefaultUserFactory implements UserFactory {
    create(data: any): User {
        const user = new User();
        Object.assign(user, data);
        return user;
    }
}