import {User} from "@dto/user/user";
import {Factory} from "@factories/factory";

export interface UserFactory extends Factory<User> {
}