import {UserWhitelistFactory} from "@factories/user/whitelist/user.whitelist.factory";
import Model, {UserWhitelist} from "@entities/user/whitelist/model";
import {injectable} from "inversify";

@injectable()
export class DefaultUserWhitelistFactory implements UserWhitelistFactory {
    create(data: any): UserWhitelist {
        return new Model(data)
    }
}