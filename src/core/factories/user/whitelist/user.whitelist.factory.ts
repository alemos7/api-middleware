import {Factory} from "@factories/factory";
import {UserWhitelist} from "@entities/user/whitelist/model";

export const UserWhitelistFactoryType = Symbol("UserWhitelistFactory")

export interface UserWhitelistFactory extends Factory<UserWhitelist> {}