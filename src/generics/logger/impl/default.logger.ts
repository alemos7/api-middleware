import winston from "winston";
import {isDevelopment} from "../../environment";
import {injectable} from "inversify";
import "reflect-metadata";
import Logger from "../index";

export interface DebugStream {
    write: (line: string) => void
}

@injectable()
export class DefaultLogger implements Logger {
    private readonly logger: winston.Logger

    constructor() {
        this.logger = winston.createLogger({
            level: DefaultLogger.getLevel(),
            format: winston.format.json(),
            transports: [
                new winston.transports.Console({
                    format: DefaultLogger.getConsoleFormat(),
                })
            ],
        });
    }

    error(msg: any): void {
        this.logger.error(msg)
    }

    warn(msg: any): void {
        this.logger.warn(msg)
    }

    info(msg: any): void {
        this.logger.info(msg)
    }

    debug(msg: any): void {
        this.logger.debug(msg)
    }

    getDebugStream(): DebugStream {
        return {
            write: (line: string) => {
                this.logger.debug("[IN] " + line.trim())
            }
        }
    }

    private static getLevel(): string {
        return isDevelopment() ? "debug" : "info"
    }

    private static getConsoleFormat() {
        return winston.format.combine(
            winston.format.colorize(),
            winston.format.timestamp(),
            winston.format.printf((info) => {
                const {
                    timestamp, level, message, ...args
                } = info;

                const ts = timestamp.slice(0, 19).replace('T', ' ');
                return `${ts} [${level}]: ${message} ${Object.keys(args).length ? JSON.stringify(args, null, 2) : ''}`;
            }),
        );
    }
}
