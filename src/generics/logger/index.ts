import winston from "winston";
import {DebugStream} from "./impl/default.logger";

export const LoggerType = Symbol('Logger');

export default interface Logger {
    error(msg: any): void
    warn(msg: any): void
    info(msg: any): void
    debug(msg: any): void
    getDebugStream(): DebugStream
}