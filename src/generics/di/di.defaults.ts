import {interfaces} from "inversify/dts/interfaces/interfaces";
import Logger, {LoggerType} from "../logger";
import {DefaultLogger} from "../logger/impl/default.logger";
import {App} from "../../app";
import {Database} from "../../database";

export const registerDefaultBindings = (container: interfaces.Container): void => {
    // bind logger
    container.bind<Logger>(LoggerType).to(DefaultLogger).inSingletonScope()
    // bind db
    container.bind<Database>(Database).toSelf().inSingletonScope()
    // bind app
    container.bind<App>(App).toSelf().inSingletonScope()
}