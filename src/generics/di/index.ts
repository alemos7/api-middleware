import {Container} from "inversify";
import {interfaces} from "inversify/dts/interfaces/interfaces";
import {registerDefaultBindings} from "./di.defaults";

class Di {
    private static container: interfaces.Container

    static getContainer(): interfaces.Container {
        if (!Di.container) {
            Di.container = new Container();
            registerDefaultBindings(Di.container)
        }
        return Di.container
    }
}

export default Di