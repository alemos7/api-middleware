import {jsonErrorHandling} from "@configuration/middleware/json.error.handling";

export const handleUseCase = (useCase: Promise<any>, req: any, res: any): void => {
    useCase.then(data => res.json(data).status(200)).catch(e => jsonErrorHandling(e, req, res));
}