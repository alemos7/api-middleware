const env = process.env.NODE_ENV || 'development';

export const isDevelopment = (): boolean => {
    return env === 'development';
}

export const isProduction = (): boolean => {
    return env === 'production';
}