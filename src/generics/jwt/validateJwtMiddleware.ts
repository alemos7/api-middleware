import { Request, Response, NextFunction } from "express";
import * as jwt from "jsonwebtoken";
import {HttpError} from "../../core/exceptions/http.error";
import HttpStatus from 'http-status-codes'

export const validateJwtMiddleware = (req: Request, res: Response, next: NextFunction) => {
    const unauthorizedError = new HttpError(HttpStatus.UNAUTHORIZED, "Unauthorized")
    if (!req.headers.authorization) {
        return res.status(unauthorizedError.status).json(unauthorizedError)
    }
    try {
        const token = (req.headers.authorization as string).replace("Bearer ", "");
        res.locals.jwtPayload = validateJwt(token)
        res.locals.authenticated = true
    } catch (error) {
        return res.status(unauthorizedError.status).json(unauthorizedError)
    }
    next();
};

export const validateJwt = (token: string): any => {
    return (jwt.verify(token, process.env.TOKEN_KEY) as any)
}