import jwt, {SignOptions} from "jsonwebtoken";

export const createJwt = (payload: object, ttl?: string) => {
    const options: SignOptions = {}
    if (ttl) {
        options.expiresIn = ttl
    }
    return jwt.sign(
        payload,
        process.env.TOKEN_KEY,
        options
    );
}