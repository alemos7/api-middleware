import express, {Application} from 'express';
import 'module-alias/register';
import bodyParser from 'body-parser';
import morgan from 'morgan';
import {inject, injectable} from "inversify";
import cors from 'cors';
import http from "http";
import defaultRouter from '@route/default';
import providerRouter from '@route/core/provider';
import userRouter from '@route/core/user';
import ssoRouter from '@route/core/sso';
import whitelistRouter from '@route/core/whitelist';
import Logger, {LoggerType} from "@generics/logger";
import helmet from "helmet";
import {isDevelopment} from "@generics/environment";

@injectable()
export class App {
    app: Application;

    constructor(@inject(LoggerType) private logger: Logger) {
        this.app = express();
        this.security();
        this.settings();
        this.routes();
    }

    private security(): void {
        /**
         * Cors will enable all origins.
         * todo: configure cors (set origins)
         */
        this.app.use(cors());

        /**
         * Helmet sets various HTTP Security headers.
         * @see: https://github.com/helmetjs/helmet/blob/master/README.md#how-it-works
         */
        this.app.use(helmet());
    }

    private settings(): void {
        this.app.use(bodyParser.json({limit: '50mb'}));
        this.app.use(bodyParser.urlencoded({limit: '50mb', extended: true}));
        if (isDevelopment()) {
            /**
             * Morgan will log all incoming requests
             * (if the app crash before express handle the request it will show nothing)
             */
            this.app.use(morgan('dev', {stream: this.logger.getDebugStream()}));
        }
    }

    private routes(): void {
        this.app.use(defaultRouter);
        this.app.use(userRouter);
        this.app.use(whitelistRouter);
        this.app.use(providerRouter);
        this.app.use(ssoRouter);
    }

    async listen(): Promise<http.Server> {
        const port = process.env.PORT || 3000
        this.logger.info("Starting App...");
        return this.app.listen(
            port,
            () => this.logger.info(`App running on 0.0.0.0:${port}`)
        );
    }
}
