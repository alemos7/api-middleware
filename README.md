# Corteva SSO System

Corteva SSO System works as middleware between APIs that need to validate user access, permissions and access roles on the resources that encompass the Corteva application universe.

## Architecture
[Diagrams](doc/architecture.md)

## Endpoints
[Documentation](doc/endpoints-sso.md)
